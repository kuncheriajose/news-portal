import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { customPutToStorage } from '../shared/StorageService';

function Register(props) {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [pwd, setPwd] = useState('');
    const [isValid, setIsvalid] = useState(true)
    const history = useHistory();


    const handleSubmit = (e) => {
        e.preventDefault();

        if (name && email && pwd) {

            if (!/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email)) {
                setIsvalid(false)
                alert("Please enter valid email");

            }


            if (!/^[A-Z][-a-zA-Z]+$/.test(name)) {
                setIsvalid(false)
                alert("Display name should not contain any Special characters");

            }

            if (!/^[A-Z][-a-zA-Z]+$/.test(pwd) && pwd.length > 20) {
                setIsvalid(false)
                alert("Password should be max 20 characters");

            }

            console.log(isValid)

            if (isValid) {

                const user = {
                    name: name,
                    email: email,
                    pwd: pwd
                }
                customPutToStorage('user', JSON.stringify(user))

                history.push("/user/login")

            }


        }
        else
            alert("Please enter all fields")
    }
    return (
        <div >

            Register Now
            <form onSubmit={handleSubmit}>
                <div className="title">Name</div>
                <div>
                    <input type="text" name="name" onChange={e => setName(e.target.value)} />
                </div>
                <div className="title">Email</div>
                <div>
                    <input type="text" name="email" onChange={e => setEmail(e.target.value)} />
                </div>
                <div className="title">Password</div>
                <div>
                    <input type="password" name="pwd" onChange={e => setPwd(e.target.value)} />
                </div>

                <Button type="submit">Register</Button>
            </form>
        </div>
    );
}

export default Register;
