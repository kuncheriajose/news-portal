import React, { useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import { customGetFromStorage, customPutToStorage } from '../shared/StorageService';

function Login(props) {
    const [email, setEmail] = useState('');
    const [pwd, setPwd] = useState('');
    const history = useHistory();


    const handleSubmit = async (e) => {
        e.preventDefault();

        if (email && pwd) {

            const user = JSON.parse(localStorage.getItem('user'))
            
            if (user.email === email && pwd === pwd) {

                await customPutToStorage('isLogined', true)

                history.push("/user/profile")

            }
            else{
                alert("User not Exist")
            }
        


        }
        else
            alert("Please enter all fields")

    }
    return (
        <div>
            <form onSubmit={handleSubmit}>
                Login Now
                <div className="title">Email</div>
                <div>
                    <input type="text" name="email" onChange={e => setEmail(e.target.value)} />
                </div>
                <div className="title">Password</div>
                <div>
                    <input type="password" name="pwd" onChange={e => setPwd(e.target.value)} />
                </div>

                <Button type="submit">Login</Button>

                Not a User? <Link to={'/user/register'}>Register Now</Link>
            </form>
        </div>
    );
}

export default Login;