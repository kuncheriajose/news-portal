import React from 'react';
import { Link } from 'react-router-dom';

function Header(props) {
    return (
        <div>
            <div className="icon"><Link to="/sections">Menu</Link></div>
            <div className="icon"><Link to="/user/login">Login</Link></div>
        </div>
    );
}

export default Header;