import React from 'react';
import { Button, Card } from 'react-bootstrap';

function Articles({ articles, loading }) {

    if (loading)
        return "Loading..."

    return (
        <>
            {articles.map((article, index) =>
                <Card>
                    <Card.Body>
                        <div key={index} onClick={() => window.open(article.url, '_blank', 'noopener', 'noreferrer')}>
                            <Card.Title>{article.title}</Card.Title>
                            <div className="abstract">{article.abstract}</div>
                            <div className="img">{article.multimedia && <img src={article.multimedia[0]?.url} />}</div>
                        </div>
                    </Card.Body>
                </Card>
            )}
        </>
    );
}

export default Articles;