import React, { useEffect, useState } from 'react';
import { ARTICLE_API } from '../shared/constants';
import { customGet } from '../shared/NetworkService';
import Articles from './Articles';

function Section(props) {

    const [articles, setArticles] = useState([])
    const [filteredArticles, setFilteredArticles] = useState([])
    const [section, setSection] = useState(props.match.params.section)
    const [loading, setLoading] = useState(true)


    useEffect(() => {

        const loadArticles = async () => {
            setLoading(true)
            const response = await customGet(`${ARTICLE_API}`)
            setArticles(response.results)
            setLoading(false)
        }
        loadArticles()
        const fil = articles.filter(article => article.section === section)
        setFilteredArticles(fil)
    }, [])

    if (filteredArticles.length == 0)
        return ("No sections Available")


    return (
        <div>
            <Articles articles={filteredArticles} loading={loading} />
        </div>
    );
}

export default Section;