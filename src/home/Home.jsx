import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { ARTICLE_API } from '../shared/constants';
import { customGet } from '../shared/NetworkService';
import Articles from './Articles';
import Header from './Header';

function Home(props) {
    const [articles, setArticles] = useState([])
    const [page, setPage] = useState(0)
    const [loading, setLoading] = useState(false)



    useEffect(() => {
        const loadArticles = async () => {
            setLoading(true)
            const response = await customGet(`${ARTICLE_API}&page=${page}`)
            setArticles(response.results)
            setLoading(false)
        }
        loadArticles()
    }, [page])

    return (
        <>
            <Header />
            <Articles articles={articles} loading={loading}/>
            {page>=1 && <Button variant="outline-primary" onClick = {() => setPage(page-1)}>Prev</Button>}
            {page>=0 && <Button variant="outline-primary" onClick = {() => setPage(page+1)}>Next</Button> }
            

        </>
    );
}

export default Home;