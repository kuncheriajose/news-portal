import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { SECTION_API } from '../shared/constants';

function Sections(props) {

    const [sections, setSections] = useState([])

    useEffect(() => {

        axios.get(SECTION_API)
            .then(response => {
                setSections(response.data.results)
            })


    }, [])
    return (
        <div>
            {sections.map((section, index) =>
                <div key={index}>
                    <Link to={`section/${section.section}`}>{section.display_name}</Link>
                </div>
            )}
        </div>
    );
}

export default Sections;