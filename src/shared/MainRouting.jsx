import React from 'react';
import { Container } from 'react-bootstrap';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import Home from '../home/Home';
import Section from '../home/Section';
import Sections from '../home/Sections';
import Edit from '../profile/Edit';
import Login from '../profile/Login';
import Profile from '../profile/Profile';
import Register from '../profile/Register';

function MainRouting() {
    return (
        <Router>
            <Container>
                <Switch>
                    <Route exact path="/" >
                        <Home />
                    </Route>
                    <Route path="/sections" >
                        <Sections />
                    </Route>
                    <Route path="/section/:section" component={Section} />
                    <Route path="/user/login" >
                        <Login />
                    </Route>
                    <Route path="/user/register" >
                        <Register />
                    </Route>
                    <Route path="/user/profile" >
                        <Profile />
                    </Route>
                    <Route path="/user/profile/edit" >
                        <Edit />
                    </Route>
                </Switch>
            </Container>
        </Router>
    );
}

export default MainRouting;