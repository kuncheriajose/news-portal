import axios from 'axios';

// Custom Wrapper class for GET HTTP Requests
export const customGet = async (url) => {
    let result =  null
    await axios.get(url)
    .then(response => {
        result = response.data
    })
    return result
}